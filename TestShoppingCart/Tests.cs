﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using ShoppingCart;

namespace TestShoppingCart
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void TestUpdateOrder_AddItem()
        {
            Basket basket = new Basket(@"{}");

            basket.UpdateOrder(@"{'Orange':5}");          
            Assert.True(basket.GetBasket().ContainsKey("Orange"));
            Assert.True(basket.GetBasket()["Orange"] == 5);
        }
        
        [Test]
        public void TestUpdateOrder_RemoveItem()
        {
            Basket basket = new Basket(@"{'Orange':1,'Apple':3}");
            
            basket.UpdateOrder(@"{'Orange':0}");
            Assert.False(basket.GetBasket().ContainsKey("Orange"));
            Assert.True(basket.GetBasket()["Apple"] == 3);
        }
        
        [Test]
        public void TestUpdateOrder_InvalidItem()
        {
            Basket basket = new Basket(@"{'Orange':1,'Apple':3}");

            Exception ex = Assert.Throws<ArgumentException>(() => basket.UpdateOrder(@"{'BigMac': 1}"));
            Assert.AreEqual("Invalid product name!\r\nParameter name: BigMac", ex.Message);
        }
        
        [Test]
        public void TestUpdateOrder_GetPriceTotal()
        {   
            Basket basket = new Basket(@"{'Orange':2,'Apple':3}");

            double price = basket.GetPriceTotal();      
            Assert.True(price == 1.95);
        }
    }
}