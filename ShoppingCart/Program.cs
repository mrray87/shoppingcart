﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace ShoppingCart
{
    
    public class Basket
    {
        // Constants:
        Dictionary<string, double> prices = new Dictionary<string, double>()
        {
            {"Orange", 0.60},
            {"Apple", 0.25}
        };
        
        // Class:
        Dictionary<string, int> basket = new Dictionary<string, int>();

        public Basket(string json)
        {
            Dictionary<string, int> order = SerizalizeOrderJson(json);
            foreach (KeyValuePair<string, int> entry in order)
            {
                basket.Add(entry.Key, entry.Value);
            }
        }

        private Dictionary<string, int> SerizalizeOrderJson(string json)
        {
            return JsonConvert.DeserializeObject<Dictionary<string, int>>(json);
        }

        public Dictionary<string, int> GetBasket()
        {
            return basket;
        }

        public void PrintBasket()
        {
            foreach (KeyValuePair<string, int> entry in basket)
            {
                Console.WriteLine("{0}: {1} items.", entry.Key, entry.Value);
            }
        }

        public void UpdateOrder(string json)
        {
            Dictionary<string, int> update = SerizalizeOrderJson(json);
            foreach (KeyValuePair<string, int> entry in update)
            {
                if (!prices.ContainsKey(entry.Key))
                {
                    throw new System.ArgumentException("Invalid product name!", entry.Key);
                }
                
                if (basket.ContainsKey(entry.Key))
                {
                    if (entry.Value > 0)
                    {
                        basket[entry.Key] = entry.Value;
                    }
                    else
                    {
                        basket.Remove(entry.Key);
                    }
                }
                else
                {
                    if (entry.Value > 0)
                    {
                        basket.Add(entry.Key, entry.Value);
                    }
                }
            }
        }

        public double GetPriceTotal()
        {
            double price = 0;
            foreach (KeyValuePair<string, int> entry in basket)
            {
                price += prices[entry.Key] * entry.Value;
            }
            return price;
        }
    }
    
    
    internal class Program
    {
        public static void Main(string[] args)
        {
            string order = @"{'Orange':5}";

            Basket basket = new Basket(order);
            
            basket.PrintBasket();
            Console.WriteLine(basket.GetPriceTotal());
            
            string update = @"{'Orange':5, 'Apple': 7}";
            
            basket.UpdateOrder(update);
            
            basket.PrintBasket();
            Console.WriteLine(basket.GetPriceTotal());
        }
    }
}