﻿using NUnit.Framework;
using ShoppingCart;
using System;
using TechTalk.SpecFlow;

namespace TestsAcceptanceShoppingCart
{
    [Binding]
    public class TestTheShoppingCartSteps
    {
        Basket basket = new Basket(@"{}");

        [Given(@"I have created a new order")]
        public void GivenIHaveCreatedANewOrder()
        {
            // Do nothing
        }

        [When(@"I add (.*) apples and (.*) oranges")]
        public void WhenIAddApplesAndOranges(int p0, int p1)
        {
            basket.UpdateOrder(
                @"{'Apple':" +
                p0.ToString() +
                ",'Orange':" +
                p1.ToString() +
                "}");
        }

        [When(@"I remove all oranges")]
        public void WhenIRemoveAllOranges()
        {
            basket.UpdateOrder(@"{'Orange':0}");
        }

        [Then(@"the final cost is (.*)")]
        public void ThenTheFinalCostIs(double p0)
        {
            double price = basket.GetPriceTotal();
            Assert.IsTrue(price == p0);
        }
    }
}



