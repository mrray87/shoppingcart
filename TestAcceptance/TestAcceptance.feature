﻿Feature: Test the shopping cart
  
  Scenario: Create and update the order and get final cost
    Given I have created a new order
    
    When I add 4 apples and 1 oranges
    Then the final cost is 1.60
    
    When I remove all oranges
    Then the final cost is 1.00